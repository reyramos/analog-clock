'use strict';

var PORT = 9000;
var DOCUMENT_STRING_TITLE = "Palms and Ponies Code Documentation";
var BOWER_DEST = "./app/lib";

var lrSnippet, modRewrite, mountFolder;

lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet;

modRewrite = require("connect-modrewrite");

mountFolder = function(connect, dir) {
	return connect["static"](require("path").resolve(dir));
};

module.exports = function(grunt) {
	require("matchdep").filter("grunt-*").concat(["gruntacular", 'node-spritesheet', 'grunt-contrib-compress']).forEach(grunt.loadNpmTasks);
	grunt.initConfig({
		yeoman: {
			app: "app",
			dist: "dist"
		},
		watch: {
			livereload: {
				files: [
					"<%= yeoman.app %>/*.html"
					, "<%= yeoman.app %>/*.template"
					, "{.tmp,<%= yeoman.app %>}/css/*.css"
					, "{.tmp,<%= yeoman.app %>}/css/*/*.css"
					, "{.tmp,<%= yeoman.app %>}/css/*.less"
					, "{.tmp,<%= yeoman.app %>}/js/*.js"
					, "{.tmp,<%= yeoman.app %>}/js/*/*.js"
					, "{.tmp,<%= yeoman.app %>}/js/*/*/*.js"
					, "{.tmp,<%= yeoman.app %>}/views/*.css"
					, "<%= yeoman.app %>/images/*.{png,jpg,jpeg}"
				],
				tasks: ["livereload", "template:dev"]
			},
			less: {
				files: ["{.tmp,<%= yeoman.app %>}/css/*.less"],
				tasks: ["less:dev", "livereload"]
			}
		},
		less: {
			dev: {
				options: {
					paths: ["<%= yeoman.app %>/css"]
				},
				files: {
					".tmp/css/main.css": "<%= yeoman.app %>/css/styles.less"
				}
			},
			dist: {
				options: {
					paths: ["<%= yeoman.app %>/css", ".tmp/css"],
					yuicompress: true
				},
				files: {
					".tmp/styles.css": "<%= yeoman.app %>/css/styles.less"
				}
			}
		},
		 cssmin: {
			 options: {
				 keepSpecialComments: 0,
				 report: true
			 },
			 minify: {
				expand: true,
				cwd: '.tmp',
				src: '*.css',
				dest: '<%= yeoman.dist %>',
				ext: '.css'
			 }
		 },
		connect: {
			options: {
				hostname: "0.0.0.0",
				port: PORT
			},
			livereload: {
				options: {
					middleware: function(connect) {
						return [
							lrSnippet
							, modRewrite([
											 "!png|jpg|gif|css|js|less|html|otf|ttf|woff$ /index.html [L]"
											 , "^/[^/]+/([^/]+/.*(png|otf|ttf|css|woff)) /$1 [L]"])
							, mountFolder(connect, ".tmp"), mountFolder(connect, "app")];
					}
				}
			},
			test: {
				options: {
					middleware: function(connect) {
						return [mountFolder(connect, ".tmp"), mountFolder(connect, "app")];
					}
				}
			},
			dist: {
				options: {
					middleware: function(connect) {
						return [
							modRewrite(["!png|jpg|gif|css|js|less|html|otf|ttf$ /index.html [L]", "^/[^/]+/([^/]+/.*(png|otf|ttf|css)) /$1 [L]"]), function(req, res, next) {
								if (req.url.split('.').pop() === 'html') {
									res.setHeader('Content-Encoding', 'gzip');
								}
								return next();
							}, mountFolder(connect, "dist")
						];
					}
				}
			}
		},
		open: {
			server: {
				url: "http://localhost:<%= connect.options.port %>/"
			}
		},
		/*Clean directories and remove any files*/
		clean: {
			dist: [".tmp", "<%= yeoman.dist %>/*"],
			server: ".tmp"
		},
		jshint: {
			options: {
				jshintrc: ".jshintrc"
			},
			all: ["Gruntfile.js", "<%= yeoman.app %>/js/*.js", "app/test/spec/*.js"]
		},
		testacular: {
			unit: {
				configFile: "karma.coffee",
				singleRun: true
			}
		},
		dox: {
			options: {
				title: DOCUMENT_STRING_TITLE
			},
			files: {
				src: ["<%= yeoman.app %>/js"],
				dest: "<%= yeoman.app %>/docs"
			}
		},
		requirejs: {
			dist: {
				options: {
					baseUrl: "app/js",
					findNestedDependencies: true,
					logLevel: 0,
					mainConfigFile: "app/js/main.js",
					name: 'main',
					onBuildWrite: function(moduleName, path, contents) {
						var modulesToExclude, shouldExcludeModule;
						modulesToExclude = ['main', 'bootstrap'];
						shouldExcludeModule = modulesToExclude.indexOf(moduleName) >= 0;
						if (shouldExcludeModule) {
							return '';
						}
						return contents;
					},
					optimize: "none",
					out: ".tmp/scripts-require.js",
					preserveLicenseComments: false,
					skipModuleInsertion: true
				}
			}
		},
		/*Concat the files from src to one dest file*/
		concat: {
			dist: {
				src: ['.tmp/scripts-require.js', 'app/js/bootstrap.js'],
				dest: ".tmp/scripts-concat.js"
			}
		},
		/*Minify files with UglifyJS.*/
		uglify: {
			options: {
				mangle: false,
				compress: true
			},
			dist: {
				files: {
					'.tmp/scripts.js': '.tmp/scripts-concat.js'
				}
			}
		},
		/*Minify HTML*/
		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					removeCommentsFromCDATA: true,
					collapseWhitespace: false
				},
				files: {
					".tmp/index.html": ".tmp/index-concat.html"
				}
			}
		},
		useminPrepare: {
			html: "<%= yeoman.app %>/index.html",
			options: {
				dest: "<%= yeoman.dist %>"
			}
		},
		/*Minify PNG and JPEG images*/
		imagemin: {
			dist: {
				files: [
					{
						expand: true,
						cwd: "<%= yeoman.app %>/images",
						src: "*.{png,jpg,jpeg}",
						dest: "<%= yeoman.dist %>/images"
					}
				]
			}
		},
		copy: {
			dist: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: "<%= yeoman.app %>",
						dest: "<%= yeoman.dist %>",
						src: ["*.html"
							  ,"img/**.{png,gif,ico,txt}"
							  ,"fonts/*/*.otf"
							  ,"fonts/*/*.ttf"
							  ,"fonts/*/*.eot"
							  ,"fonts/*/*.svg"
							  ,"fonts/*/*.woff"
							  ,"fonts/**.woff"
							  ,"fonts/**.otf"
							  ,"fonts/**.ttf"
							  ,"fonts/**.eot"
							  ,"fonts/**.svg"
							  ,"fonts/**.woff"
							  ,"js/vendor/*.js"
							  ,"views/**.html"
							  ,"views/**.php"]
					}
				]
			},
			dist2: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: ".tmp",
						dest: "<%= yeoman.dist %>",
						src: ["img/*.png", "*.html","*.js","*.css" ]
					}
				]
			},
			server: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: "<%= yeoman.dist %>",
						dest: ".tmp",
						src: ["css/sprite.less", "img/sprite.png|ttf", "img/sprite2x.png"]
					}
				]
			}
		},
		/*template task is part of grunt-hustler*/
		template: {
			dev: {
				files: {
					".tmp/index.html": "<%= yeoman.app %>/index.template"
				},
				environment: "dev"
			},
			dist: {
				files: {
					".tmp/index-concat.html": "<%= yeoman.app %>/index.template"
				},
				environment: "dist",
				css_sources: '<%= grunt.file.read(".tmp/styles.css") %>',
				js_sources: '<%= grunt.file.read(".tmp/scripts.js") %>'
			}
		},
		bower: {
			install: {
				options: {
					targetDir: BOWER_DEST,
					verbose: true,
					install: true,
					cleanup: true
				}
			}
		}
	});
	grunt.renameTask("regarde", "watch");
	grunt.registerTask("server", function(target) {
		if (target === "dist") {
			return grunt.task.run(["livereload-start", "connect:dist:keepalive", "open"]);
		} else {
			return grunt.task.run([
				"copy"
				, "livereload-start"
				, "template:dev"
				, "connect:livereload"
				, "open"
				, "watch"
			]);
		}
	});
	grunt.registerTask("server-only", function(target) {
		return grunt.task.run(["open", "watch"]);
	});
	grunt.registerTask("test", [
		"clean:server"
		, "testacular"]);

	grunt.registerTask("build", [
		"clean:dist"
		, "requirejs"
		, "concat:dist"
		, "uglify"
		, "less:dist"
		, "imagemin"
		, "template:dist"
		, "htmlmin"
		, "copy:dist"
		, "copy:dist2"
		, "cssmin"
	]);
	return grunt.registerTask("default", ["build"]);
};

