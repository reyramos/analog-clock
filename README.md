# Boilerplate Mobile WebApp #

## About ##
HTML5 Boilerplate with the following support for webapp
RequireJs
LessJs
AngularJs
BowerJs

This webapp creates and loads necessary files your application, development, and
production support.  Compiles css, js and adds the compile file path to index.html.
All the pages views are saved in views, folder for file inclusion in index

## Current Features ##

  * Initial scaffolding and documentation!
  * Allow gzip compressing in dist/
  * Modified .htaccess to allow page refresh for Angular JS HTML5 pushState support
     HTML5 pushState support
  	 This enables urls to be routed with HTML5 pushState so they appear in a
  	 '/someurl' format without a page refresh
  	 The server must support routing all urls to index.html as a catch-all for
  	 this to function properly,
  	 The alternative is to disable this which reverts to '#!/someurl'
  	 anchor-style urls.
  	 $locationProvider.html5Mode(true)
  * SpriteSheet Documentation comming soon <http://emlyn.net/posts/spritesmith-tutorial>

## Requirements ##

  * Node.js <http://nodejs.org/>

## Development Environment Setup ##

  1. Install Node.JS
	
	Use your system package manager (brew,port,apt-get,yum etc)

  2. Install global Bower, Grunt and PhantomJS commands, this step is not necessary,
  because once you perform grunt build it will install all the necessary files from package.json

	  ``bash
		sudo npm install -g grunt-cli bower phantomjs
	  ``


## Documentation Generation ##
  
	1. Generate documentation with grunt
	  ``bash
	  grunt dox
	  ``

## Development Work-flow ##

  1. Download necessary libraries from bower.js file
	  bower install

  2. Build Development/Distribution
	  ``bash
	  grunt
	  or
	  grunt build
	  ``

  2. Make changes (reflected live in browser) while in development.
     If you make changes and want to reflect in distribution run step 1


  3. Run unit tests
	 Still trying to learn and practice unit testing during the meantime read Angular JS <http://angularjs.org/>

## Deployment ##

  1. Run build scripts

	  ``bash
	  grunt build
		``

  2. Serve compiled, production-ready files from the 'dist' directory
	  ``bash
	  grunt server:dist
		``

##VirtualHost##
    <Directory /var/www>
    #add the following to allow decompression
    AddEncoding x-gzip .html
    </Directory>

## AWESOME DOCUMENTATION ##

    * Javascript NPM style guide
		- https://npmjs.org/doc/coding-style.html

	* editorConfig plugin
		- http://editorconfig.org/


	* CSS/HTML by Googles Style guide
		- http://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml

## PLEASE TAKE THIS FOR LEARNING PURPOSE AND YOUR OWN DEVELOPMENT ##












