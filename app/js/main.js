// Require JS  Config File
require({
		baseUrl: 'js/',
		paths: {
			 'angular': '../lib/angular/index'
			, 'jquery': '../lib/jquery/jquery'
			, 'angular-resource': '../lib/angular-resource/index'
			, 'jquery-counter': '../lib/jquery-counter/src/jquery.counter'
		},
		shim: {
			'app': {
				'deps': [
					'angular'
					, 'angular-resource'
					, 'jquery-counter'
				]
			},
			'routes': { 'deps': [
				'app'
			]},
			'angular-resource': { 'deps': ['angular', 'jquery'], 'exports': 'angular' },
			'jquery-counter': { 'deps': ['jquery']},
			'controllers/ApplicationController': {
				'deps': [
					'app'
				]}
		}
	},
	[
		'require'
		, 'routes'
		, 'controllers/ApplicationController'

	],
	function (require) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);
