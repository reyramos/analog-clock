/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController'
	,[
		  '$rootScope'
		, '$http'
		, '$scope'
		, function
		(
			$rootScope
			, $http
			, $scope
			) {

		$rootScope.siteName = 'Analog Clock'


		setInterval(function(){
			$scope.$apply(function(){
				var now = new Date()
					,h = now.getHours()

				$scope.time = {
					hour:h >= 12 ? h-12 : h
					, minute:now.getMinutes()
					, second:now.getSeconds()
				}

				//every minute and second are 6degrees
				angular.element(document.getElementById('minute')).css({"-webkit-transform": "rotate(" + ($scope.time.minute * 6) + "deg)"});
				angular.element(document.getElementById('second')).css({"-webkit-transform": "rotate(" + ($scope.time.second * 6) + "deg)"});
				//every hour is 30degrees
				hour = ($scope.time.hour * 30) + (360 / ($scope.time.minute * 6))

				angular.element(document.getElementById('hour')).css({"-webkit-transform": "rotate(" + hour + "deg)"});
			})
		},1000)



		$scope.$on('$routeChangeSuccess', function(){
		})

		$scope.$on('$routeChangeStart', function(){
		})

	}]
)
